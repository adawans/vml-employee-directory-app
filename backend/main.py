from google.appengine.api import memcache
import webapp2

import logging

class Data(webapp2.RequestHandler):
    def get(self):
        self.response.headers.add_header("Access-Control-Allow-Origin", self.request.headers.get('Origin', '*'))
        self.response.headers.add_header("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
        self.response.headers.add_header("Access-Control-Allow-Credentials", "true");
        self.response.headers['Content-Type'] = 'application/json'

        data = open("people.json")
        self.response.out.write(data.read())
        data.close()

app = webapp2.WSGIApplication([('/data', Data)],
                              debug=False)