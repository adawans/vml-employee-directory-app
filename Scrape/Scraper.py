from bs4 import BeautifulSoup
import urllib
import urllib2
import json
import Image

DOMAIN = "http://wiki.vmlsa.com"
YATS_FOLDER = "//192.168.0.48/yatsLIVE/public/directory/assets"
# YATS_FOLDER = "../vml_directory_app/src/assets"
TRIBES = ["Gaia", "Modjadji", "Phoenix", "Arashi", "Medallion", "Clay Collab"];

def parse_main():
	people = []
	tribes = {}
	departments = {}
	html = urllib2.urlopen(DOMAIN + '/wiki/index.php/VMLSA_phone_extensions')
	soup = BeautifulSoup(html, 'html.parser')

	tables = soup.find_all('table')
	for i in xrange(len(tables)):
		parse_table(tables[i], ["Johannesburg", "Cape Town"][i], people, tribes, departments)

	print tribes.keys()
	print list(sorted(departments.keys()))

	old_peeps = {}
	with open(YATS_FOLDER + "/data/people.json", 'r') as old_f: 
		old = json.load(old_f)
		for i in range(len(old)):
			old_peeps[old[i]["email"]] = old[i]["email"]

	for i in range(len(people)):
		people[i]["isNew"] = not people[i]["email"] in old_peeps

	with open(YATS_FOLDER + "/data/people.json", 'w') as f:
		f.write(json.dumps(people))

def parse_table(table, city, people, tribes, departments):
	for row in table.find_all('tr'):
		cells = row.find_all('td')
		if len(cells) >= 9:
			person = {}
			person["name"] = cells[0].get_text().strip()
			person["extension"] = cells[1].get_text().strip()
			person["mobile"] = cells[2].get_text().strip()
			person["email"] = cells[3].get_text().strip()
			person["gmail"] = cells[4].get_text().strip()

			index = 6 if len(cells) > 10 else 5

			person["dob"] = cells[index].get_text().strip()
			person["title"] = cells[index + 1].get_text().strip()
			person["department"] = departmentCleanup(cells[index + 2].get_text().strip())
			person["tribe"] = tribeCleanup(cells[index + 3].get_text().strip())
			person["city"] = city

			tribes[person["tribe"]] = person["tribe"]
			departments[person["department"]] = person["department"]

			retrieve_image(cells[index + 4], person["email"])
			people.append(person)


def retrieve_image(row, email):
	anchor = row.find('a')
	if anchor:
		href = anchor.get("href")
		html = urllib2.urlopen(DOMAIN + href)
		soup = BeautifulSoup(html, 'html.parser')
		anchorInner = soup.select("div.fullImageLink a")
		if len(anchorInner) > 0:
			filename = email.split("@")[0].lower() + ".jpg"
			try:
				urllib.urlretrieve(DOMAIN + anchorInner[0].get("href"), "images/original/" + filename)
				im = Image.open("images/original/" + filename)
				im = standard_ratio(im, 512)
				im.save(YATS_FOLDER + "/imgs/people/large/" + filename, "JPEG")
				im.thumbnail((256, 256), Image.ANTIALIAS)
				im.save(YATS_FOLDER + "/imgs/people/small/" + filename, "JPEG")
			except Exception as e:
				pass
			

def standard_ratio(image, size):
	w, h = image.size
	min_dim = min(w, h)
	x_pad = int((w - min_dim) * 0.5)
	y_pad = int((h - min_dim) * 0.5)
	image = image.crop((x_pad, y_pad, w - x_pad, h - y_pad))
	image.thumbnail((size, size), Image.ANTIALIAS)
	return image

def tribeCleanup(tribe):
	tribe = "Clay Collab" if tribe == "Clay Colab" else tribe
	return tribe if tribe in TRIBES else ''

def departmentCleanup(department):
	department = "CODE" if department.upper() == "CODE" else department
	department = "Creative" if department == "Creaitive" else department
	department = "Insight" if department == "Insigh" else department
	department = "Management" if department == "Managemtent" else department
	department = "People Operations" if department == "People Operation" else department
	department = "Q Division" if department == "QIS" else department
	department = "Strategy" if department == "Stratefy" else department
	return department


parse_main()