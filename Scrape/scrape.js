var tables = document.getElementsByTagName("table");
var cities = ["Johannesburg", "Cape Town"];
var people = [];

function toDataURL (img) {
	var canvas = document.createElement('canvas'), context = canvas.getContext('2d');
	canvas.width = img.width;
	canvas.height = img.height;
	context.drawImage(img, 0, 0, img.width, img.height);
	var result = canvas.toDataURL();
	canvas.remove;

	return result;
}

function saveImage (img, name, order) {
	var id = "hiddenLink" + name;
	var child = document.createElement('div');
	child.innerHTML = "<a id='" + id + "' href='" + img.src + "' download='" + name + ".jpg'>Download</a>";
	child = child.firstChild;
	document.getElementsByTagName("body")[0].appendChild(child);
	setTimeout(function(){
		console.log(id);
		document.getElementById(id).click();
	}, order * 200);
	
	//document.getElementById('hiddenLink').parentElement.removeChild(document.getElementById('hiddenLink'));
}

var dCount = 1;


for (var i = 0; i < tables.length; i++) {
	var rows = tables[i].getElementsByTagName("tr");
	for (var j = 1; j < rows.length; j++) {
		var row = rows[j];
		if (row.cells.length >= 9) {
			var person = {};
			var index = 0;
			person["name"] = row.cells[index++].textContent;
			person["extension"] = row.cells[index++].textContent;
			person["mobile"] = row.cells[index++].textContent;
			person["email"] = row.cells[index++].textContent;
			person["gmail"] = row.cells[index++].textContent;
			if (row.cells.length > 9) {
				index ++;
			}
			person["dob"] = row.cells[index++].textContent;
			person["title"] = row.cells[index++].textContent;
			person["tribe"] = row.cells[index++].textContent;
			person["city"] = cities[i];
			//person["photo"] = row.cells[8].textContent;

			var img = row.cells[index].children[0].children[0];
			var imgName = person["email"].trim().split("@")[0];
			if (img) {
				saveImage(img, imgName, dCount);
				dCount += 1;
			}
			
			people.push(person);
		}
	}
}

console.log(JSON.stringify(people));