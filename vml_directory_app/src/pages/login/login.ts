import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { ListPage } from '../list/list';

@Component({
	selector: 'page-login',
	templateUrl: 'login.html',
})
export class LoginPage {

	public username: string;
	public password: string;
	public credentials: FormGroup;

	constructor(public navCtrl: NavController, public navParams: NavParams, public authProv: AuthProvider) {
		this.credentials = new FormGroup({
		   username: new FormControl(null, Validators.compose([Validators.required, Validators.minLength(5)])),
		   password: new FormControl(null, Validators.compose([Validators.required, Validators.minLength(5)]))
		});
	}

	submitLogin() {
		this.authProv.authenticate(this.username, this.password).subscribe(data => {
			this.navCtrl.setRoot(ListPage, {}, {animate: true, direction: 'up'});
		});
	}

}
