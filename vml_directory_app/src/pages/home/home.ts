import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ListPage } from '../list/list';
import { PeopleProvider } from '../../providers/people/people';

@Component({
	selector: 'page-home',
	templateUrl: 'home.html',
})
export class HomePage {

	public items: any;
	public filter: string;
	private _peopleProv: PeopleProvider;

	constructor(public navCtrl: NavController, public navParams: NavParams, public peopleProv: PeopleProvider) {
		this.items = navParams.get('items');
		this.filter = navParams.get('filter');
		this._peopleProv = peopleProv;

		if (!this.items) {
			this.items = [
				{
					"title" : "Everyone"
				},
				{
					"title" : "City",
					"filter" : "city",
					"items" : this.listToItems(peopleProv.cities)
				},
				{
					"title" : "Tribe",
					"filter" : "tribe",
					"items" : this.listToItems(peopleProv.tribes)
				},
				{
					"title" : "Department",
					"filter" : "department",
					"items" : this.listToItems(peopleProv.departments)
				}
			];
		}
	}

	listToItems(list) {
		return list.map(i => {
			return {
				"title" : i 
			};
		});
	}

	itemTapped(item) {
		if (item.items) {
			this.navCtrl.push(HomePage, {
				items: item.items,
				filter: item.filter,
			});
		} else {
			this._peopleProv.clearFilters();
			if (this.filter) {
				this._peopleProv[this.filter] = item.title;
			}

			this.navCtrl.push(ListPage);

		}
		
	}

}
