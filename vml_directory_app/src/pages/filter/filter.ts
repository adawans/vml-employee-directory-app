import { Component } from '@angular/core';

import { ViewController } from 'ionic-angular';

import { PeopleProvider } from '../../providers/people/people';

@Component({
	selector: 'page-filter',
	templateUrl: 'filter.html'
})
export class FilterPage {
	constructor(public peopleProv: PeopleProvider, public viewCtrl: ViewController) {

	}

	close() {
		this.viewCtrl.dismiss();
	}
}
