import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ModalController, LoadingController, Content } from 'ionic-angular';
import { ItemDetailsPage } from '../item-details/item-details';
import { PeopleProvider } from '../../providers/people/people';
import { FilterPage } from '../filter/filter';

@Component({
	selector: 'page-list',
	templateUrl: 'list.html'
})
export class ListPage {
	@ViewChild(Content) content: Content;

	private _people: any;
	private _page = 1;

	constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public loadingCtrl: LoadingController, public peopleProv: PeopleProvider) {
		this._people = [];

		this.peopleProv.search = null;

		let loader = this.loadingCtrl.create({
			content: "Please wait...",
			showBackdrop: false
		});
		loader.present();

		peopleProv.getPeople().subscribe(data => {
			this._people = data;
			loader.dismiss();
		});
	}

	getPeople() {
		return this.peopleProv.applyAllFilters(this._people).slice(0, this._page * 40);
	}

	getDescription(person) {
		return [person.title, person.tribe || person.department, person.city].filter(p => p).join(" | ");
	}

	searchChanged(event) {
		this.peopleProv.search = event.target.value ? event.target.value.toLowerCase().trim() : null;
	}

	itemTapped(event, item) {
		this.navCtrl.push(ItemDetailsPage, {
			item: item
		});
	}

	presentFilter(event) {
		let modal = this.modalCtrl.create(FilterPage);
		modal.present({
			ev: event
		});
	}

	loadMore(event) {
		this._page += 1;
		event.complete();
	}

	scrollUp() {
		this.content.scrollToTop();
	}
}
