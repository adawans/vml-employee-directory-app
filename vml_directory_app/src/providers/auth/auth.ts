import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import { User } from '../../models/user';

@Injectable()
export class AuthProvider {

	private _user: User = null;
	
	constructor(public http: HttpClient) {
	}

	authenticate(username, password) {
		return Observable.create(observer => {
			observer.next(this.generateUser());
		});
	}

	getAuthenticatedUser() {
		if (!this._user) {
			let storedUser = localStorage.getItem("user");
			if (storedUser) {
				this._user = JSON.parse(storedUser);
			}
		}

		return this._user;
	}

	generateUser() {
		var user = new User();
		user.name = "Bob";
		user.surname = "Bobbles";
		user.token = "JKHGSDFUIFS654SDFbhkjgh";

		this._user = user;
		localStorage.setItem("user", JSON.stringify(user));

		return user;
	}

}
