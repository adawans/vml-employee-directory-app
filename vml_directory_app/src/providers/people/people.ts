import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/publishReplay';
import { Person } from '../../models/person';

@Injectable()
export class PeopleProvider {

  private _people: any;
  public departments: any;
  public tribes: any;
  public cities: any;
  public search: string;
  public department: string;
  public tribe: string;
  public city: string;
  public isNew: boolean;

  constructor(public http: HttpClient) {
  	this._people = null;

    this.departments = ['Account Management','CODE','Client Engagement','Creative','Finance','IT','Insight','Management','Media','OPS','OPS/Production','Office Ops','Ops & Traffic','People Operations','Production','Project Management','Q Division','Social Media','Strategy','Traffic','UX'].sort();
    this.tribes = ["Gaia", "Modjadji", "Phoenix", "Arashi", "Medallion", "Clay Collab"].sort();
    this.cities = ["Johannesburg", "Cape Town"].sort();

    this.clearFilters();
  }

  getPeople()  {
  	if (!this._people) {
  		//this._people = this.http.get('https://randomuser.me/api/?results=100')
      this._people = this.http.get('./assets/data/people.json')
                                   .map(res => (res as Array<any>).sort((a,b) => a.name.localeCompare(b.name)).map(r => {
                                      let result = new Person(r);
                                      return result;
                                    }))
                                   .publishReplay(1)
                                   .refCount();
  	}
  	return this._people;
  }

  applyAllFilters(peeps) {
    peeps = this.applySearch(peeps);
    peeps = this.applyDepartment(peeps);
    peeps = this.applyTribe(peeps);
    peeps = this.applyCity(peeps);
    peeps = this.applyIsNew(peeps);
    return peeps;
  }

  applySearch(peeps) {
    if (this.search && this.search.length > 0) {
      peeps = peeps.filter((p) => {
        let name = p.getFullName().toLowerCase();
        return name.indexOf(this.search) >= 0;
      });
    }
    return peeps;
  }

  applyDepartment(peeps) {
    if (this.department && this.department.length > 0) {
      peeps = peeps.filter((p) => {
        return p.department == this.department;
      });
    }
    return peeps;
  }

  applyTribe(peeps) {
    if (this.tribe && this.tribe.length > 0) {
      peeps = peeps.filter((p) => {
        return p.tribe == this.tribe;
      });
    }
    return peeps;
  }

  applyCity(peeps) {
    if (this.city && this.city.length > 0) {
      peeps = peeps.filter((p) => {
        return p.city == this.city;
      });
    }
    return peeps;
  }

  applyIsNew(peeps) {
    if (this.isNew) {
      peeps = peeps.filter((p) => {
        return p.isNew;
      });
    }
    return peeps;
  }

  clearFilters() {
    this.department = "";
    this.tribe = "";
    this.city = "";
    this.isNew = false;
  }

}
