export class Person {

    public name: string;
    public mobile: string;
    public email: string;
    public extension: string;
    public title: string;
    public dob: string;
    public department: string;
    public tribe: string;
    public city: string;
    public isNew: boolean;
 
    constructor(public data: any) {
        this.data = data;
        this.name = data.name;
        this.mobile = data.mobile;
        this.email = data.email || "";
        this.extension = data.extension
        this.dob = data.dob;
        this.title = data.title;
        this.tribe = data.tribe ? data.tribe.trim() : null;
        this.department = data.department ? data.department.trim() : null;
        this.city = data.city ? data.city.trim() : null;
        this.isNew = data.isNew;
    }
 
    getFullName() {
        return this.data.name;
    }

    getThumbnail() {
        if (this.data.email && this.data.email.length > 5) {
            return "./assets/imgs/people/small/" + this.data.email.trim().split("@")[0].toLowerCase() + ".jpg";
        }
        return null;
    }

    getPicture() {
        if (this.data.email && this.data.email.length > 5) {
            return "./assets/imgs/people/large/" + this.data.email.trim().split("@")[0].toLowerCase() + ".jpg";
        }
        return null;
    }
 
}